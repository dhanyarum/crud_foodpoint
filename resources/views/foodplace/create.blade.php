@extends('layouts.app')

@section('content')
<h4>Food Place Baru</h4>
<form action="{{ route('foodplace.store') }}" method="post">
    {{csrf_field()}}
    <div class="form-group {{ $errors->has('nama') ? 'has-error' : '' }}">
        <label for="nama" class="control-label">Nama</label>
        <input type="text" class="form-control" name="nama" placeholder="Nama Restoran">
        @if ($errors->has('nama'))
            <span class="help-block">{{ $errors->first('nama') }}</span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('alamat') ? 'has-error' : '' }}">
        <label for="alamat" class="control-label">Alamat</label>
        <textarea name="alamat" cols="30" rows="5" class="form-control"></textarea>
        @if ($errors->has('alamat'))
            <span class="help-block">{{ $errors->first('alamat') }}</span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('latitude') ? 'has-error' : '' }}">
        <label for="latitude" class="control-label">Latitude</label>
        <input type="text" class="form-control" name="latitude" placeholder="Latitude">
        @if ($errors->has('latitude'))
            <span class="help-block">{{ $errors->first('latitude') }}</span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('longtitude') ? 'has-error' : '' }}">
        <label for="longtitude" class="control-label">Longitude</label>
        <input type="text" class="form-control" name="longtitude" placeholder="Longtitude">
        @if ($errors->has('longtitude'))
            <span class="help-block">{{ $errors->first('longtitude') }}</span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('id_menu') ? 'has-error' : '' }}">
        <label for="id_menu" class="control-label">ID Menu</label>
        <input type="text" class="form-control" name="id_menu" placeholder="ID MENU">
        @if ($errors->has('id_menu'))
            <span class="help-block">{{ $errors->first('id_menu') }}</span>
        @endif
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-info">Simpan</button>
        <a href="{{ route('foodplace.index') }}" class="btn btn-default">Kembali</a>
    </div>
</form>
@endsection