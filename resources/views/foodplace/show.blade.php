@extends('layouts.app')

@section('content')
<h4>{{ $foodplace->nama }}</h4>
<p>{{ $foodplace->alamat }}</p>
<p>{{ $foodplace->latitude }}</p>
<p>{{ $foodplace->longtitude }}</p>
<p>{{ $foodplace->id_menu }}</p>
<a href="{{ route('foodplace.index') }}" class="btn btn-default">Kembali</a>
@endsection