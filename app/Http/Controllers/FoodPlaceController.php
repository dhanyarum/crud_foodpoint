<?php

namespace App\Http\Controllers;

use App\FoodPlace;
use Illuminate\Http\Request;

class FoodPlaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $foodplaces = FoodPlace::orderBy('id', 'DESC')->paginate(5);
        return view('foodplace.index', compact('foodplaces'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('foodplace.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request, [
            'nama' => 'required',
            'alamat' => 'required',
            'latitude' => 'required',
            'longtitude' => 'required',
            'id_menu' => 'required'
        ]);

        $foodplace = FoodPlace::create($request->all());

        return redirect()->route('foodplace.index')->with('message', 'Food Place berhasil Diinputkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FoodPlace  $foodPlace
     * @return \Illuminate\Http\Response
     */
    public function show(FoodPlace $foodplace)
    {
      
        return view('foodplace.show', compact('foodplace'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FoodPlace  $foodPlace
     * @return \Illuminate\Http\Response
     */
    public function edit(FoodPlace $foodplace)
    {

        return view('foodplace.edit', compact('foodplace'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FoodPlace  $foodPlace
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FoodPlace $foodplace)
    {
        $this->validate($request, [
            'nama' => 'required',
            'alamat' => 'required',
            'latitude' => 'required',
            'longtitude' => 'required',
            'id_menu' => 'required'
        ]);

        $foodplace->update($request->all());

        return redirect()->route('foodplace.index')->with('message', 'Food Place berhasil diubah!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FoodPlace  $foodPlace
     * @return \Illuminate\Http\Response
     */
    public function destroy(FoodPlace $foodplace)
    {
        $foodplace->delete();
        return redirect()->route('foodplace.index')->with('message', 'Food Place berhasil dihapus!');
    }
}
