<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FoodPlace extends Model
{
    use SoftDeletes;
    protected $fillable=
    [
    	'nama','alamat', 'latitude', 'longtitude', 'id_menu'
    ];
    protected $dates= ['deleted_at'];
}
